# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.5] - 2020-05-09
- remove plugin specific .eslintrc

## [0.0.4] - 2020-04-25
- fix webpack-stats.json path

## [0.0.3] - 2020-04-25
- fix version receiving

## [0.0.2] - 2020-04-25
- API updates

## [0.0.1] - 2020-03-29
- initial version
