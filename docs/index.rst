
GDAPS - Vue.js frontend
=======================

This is a GDAPS plugin, which provides a Vue.js frontend support to any
GDAPS/Django application

.. toctree::
    :maxdepth: 2
    :caption: Table of Contents

    Installation
    usage
    APIs
    Contributing


License
=======

I'd like to give back what I received from many Open Source software packages, and keep this
library as open as possible, and it should stay this way.
GDAPS is licensed under the `General Public License, version 3 <https://www.gnu.org/licenses/gpl-3.0-standalone.html>`_.
